package com.example.shoppinglistchallenge;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class SecondActivity extends AppCompatActivity {

    public static final String EXTRA_NEW_ITEM = "";
    private static final String LOG_TAG = SecondActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }


    public void addCheese(View view) {
        String newItem = "Cheese";
        Intent replyIntent = new Intent();
        replyIntent.putExtra(EXTRA_NEW_ITEM, newItem);
        setResult(RESULT_OK, replyIntent);
        Log.d(LOG_TAG, "End SecondActivity");
        finish();
    }

    public void addApples(View view) {
        String newItem = "Apples";
        Intent replyIntent = new Intent();
        replyIntent.putExtra(EXTRA_NEW_ITEM, newItem);
        setResult(RESULT_OK, replyIntent);
        Log.d(LOG_TAG, "End SecondActivity");
        finish();
    }

    public void addRice(View view) {
        String newItem = "Rice";
        Intent replyIntent = new Intent();
        replyIntent.putExtra(EXTRA_NEW_ITEM, newItem);
        setResult(RESULT_OK, replyIntent);
        Log.d(LOG_TAG, "End SecondActivity");
        finish();
    }

    public void addMilk(View view) {
        String newItem = "Milk";
        Intent replyIntent = new Intent();
        replyIntent.putExtra(EXTRA_NEW_ITEM, newItem);
        setResult(RESULT_OK, replyIntent);
        Log.d(LOG_TAG, "End SecondActivity");
        finish();
    }
}