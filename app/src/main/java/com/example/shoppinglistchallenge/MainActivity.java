// Christopher Chitwood
// CSC 515
// 9/13/20

package com.example.shoppinglistchallenge;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "";
    private static final int ITEM_REQUEST = 1;

    // I'll be using an int and array to keep track of the list of item elements.
    // There's only 10 items, so no need to go over the moon with this.
    protected int i = 0;
    private TextView[] items = new TextView[10];
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        items[0] = findViewById(R.id.item1);
        items[1] = findViewById(R.id.item2);
        items[2] = findViewById(R.id.item3);
        items[3] = findViewById(R.id.item4);
        items[4] = findViewById(R.id.item5);
        items[5] = findViewById(R.id.item6);
        items[6] = findViewById(R.id.item7);
        items[7] = findViewById(R.id.item8);
        items[8] = findViewById(R.id.item9);
        items[9] = findViewById(R.id.item10);


       // Restore from savedInstanceState
       if (savedInstanceState != null){
          boolean isVisible = savedInstanceState.getBoolean("state_visible");
           if (isVisible) {
                int numOfItems = savedInstanceState.getInt("numOfItems");
                for(int j = 0; j < numOfItems; j++){
                   String[] restoredArray = savedInstanceState.getStringArray("savedArray");
                   String restoredString = restoredArray[j].toString();
                   items[j].setText(restoredString);
                   items[j].setVisibility(View.VISIBLE);
                  // Log.d(LOG_TAG, "RESTORED THIS STRING: " + restoredString);
                }
            }
        }
    }

    public void launchSecondActivity(View view) {
        Log.d(LOG_TAG, "Button clicked");
        Intent intent = new Intent(this, SecondActivity.class);
        startActivityForResult(intent, ITEM_REQUEST);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode,  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Wrap back around to the beginning of the list.
        if (i == 10)
            i = 0;

        // Receive the intent extra from the second activity and set it to the current item the list.
        if (requestCode == ITEM_REQUEST) {
            if (resultCode == RESULT_OK) {
                String newItem = data.getStringExtra(SecondActivity.EXTRA_NEW_ITEM);
                Log.d(LOG_TAG, "added" + newItem);
                items[i].setText(newItem);
                items[i].setVisibility(View.VISIBLE);
            }
        }
        i++;  // Move to the next item in the index.
    }

    @Override
    public void onStart(){
        super.onStart();
        Log.d(LOG_TAG, "onStart");
    }

    @Override
    public void onPause(){
        super.onPause();
        Log.d(LOG_TAG, "onPause");
    }

    @Override
    public void onRestart(){
        super.onRestart();
        Log.d(LOG_TAG, "onRestart");
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.d(LOG_TAG, "onResume");
    }

    @Override
    public void onStop(){
        super.onStop();
        Log.d(LOG_TAG, "onStop");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
       super.onSaveInstanceState(outState);

        String[] savingArray = new String[10];
        // Run through the items and record if they are visible and their values.
        for (int j = 0; j < items.length; j++ ){
            if (items[j].getVisibility() == View.VISIBLE) {
                outState.putBoolean("state_visible", true);
                //outState.putString("state_text",items[j].getText().toString());
                outState.putInt("numOfItems", i);
                savingArray[j] = items[j].toString();
                outState.putStringArray("savedArray", savingArray);
            }
        }

    }


}